<?php

namespace Drupal\vb_share\Plugin\Block;

use Drupal\Core\Url;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\Cache;

/**
 * Provides a 'Social sharing' block.
 *
 * @Block(
 *  id = "social_sharing",
 *  admin_label = @Translation("Social sharing"),
 * )
 */
class SocialSharing extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $count = 0;

    $title = '';
    $node = \Drupal::routeMatch()->getParameter('node');
    if(!is_object($node) && $node != NULL) {
      $node = \Drupal::entityTypeManager()->getStorage('node')->load($node);
    }
    if(is_object($node)) {
      $title = $node->getTitle();
    }

    if($this->configuration['facebook']) {
      $build['facebook'] = [
        '#type' => 'link',
        '#title' => $this->t('Deel op Facebook'),
        '#url' => Url::fromUri('https://www.facebook.com/sharer.php?u=' . \Drupal::request()->getSchemeAndHttpHost() . \Drupal::request()->getRequestUri()),
        '#attributes' => [
          'class' => ['share__link', 'share__link--facebook']
        ]
      ];
      $count++;
    }

    if($this->configuration['twitter']) {
      $build['twitter'] = [
        '#type' => 'link',
        '#title' => $this->t('Deel op Twitter'),
        '#url' => Url::fromUri('https://twitter.com/intent/tweet?url=' . \Drupal::request()->getSchemeAndHttpHost() . \Drupal::request()->getRequestUri()),
        '#attributes' => [
          'class' => ['share__link', 'share__link--twitter']
        ]
      ];
      $count++;
    }

    if($this->configuration['pinterest']) {
      $build['pinterest'] = [
        '#type' => 'link',
        '#title' => $this->t('Deel op Pinterest'),
        '#url' => Url::fromUri('https://www.pinterest.com/pin/create/button/?url=' . \Drupal::request()->getSchemeAndHttpHost() . \Drupal::request()->getRequestUri() . '&description=' . $title),
        '#attributes' => [
          'class' => ['share__link', 'share__link--pinterest']
        ]
      ];
      $count++;
    }

    if($this->configuration['linkedin']) {
      $build['linkedin'] = [
        '#type' => 'link',
        '#title' => $this->t('Deel op Linked In'),
        '#url' => Url::fromUri('https://www.linkedin.com/shareArticle?mini=true&url=' . \Drupal::request()->getSchemeAndHttpHost() . \Drupal::request()->getRequestUri() . '&title=' . $title),
        '#attributes' => [
          'class' => ['share__link', 'share__link--linkedin']
        ]
      ];
      $count++;
    }

    if($this->configuration['whatsapp']) {
      // Whatsapp is built as an inline template because the link element XSS filter strips the whatsapp:// scheme
      $build['whatsapp'] = [
        '#type' => 'inline_template',
        '#template' => '<a class="share__link share__link--whatsapp" href="whatsapp://send?text={{ title }}: {{ link }}">{% trans %}Deel op Whatsapp{% endtrans %}</a>',
        '#context' => [
          'title' => $title,
          'link' =>  \Drupal::request()->getSchemeAndHttpHost() . \Drupal::request()->getRequestUri()
        ]
      ];
      $count++;
    }

    $build['#attributes']['class'][] = 'share';
    $build['#attributes']['class'][] = 'share--count-' . $count;

    $build['#attached']['library'][] = 'vb_share/share';

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'label_display' => FALSE,
      'facebook' => 1,
      'twitter' => 1,
      'pinterest' => 1,
      'linkedin' => 1,
      'whatsapp' => 1
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node instanceof \Drupal\node\NodeInterface) {
      return Cache::mergeTags(parent::getCacheTags(), array('node:' . $node->id()));
    }
    else {
      return parent::getCacheTags();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), array('route'));
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    $form['facebook'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Facebook'),
      '#default_value' => isset($config['facebook']) ? $config['facebook'] : 0,
    ];

    $form['twitter'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Twitter'),
      '#default_value' => isset($config['twitter']) ? $config['twitter'] : 0,
    ];

    $form['linkedin'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Linked in'),
      '#default_value' => isset($config['linkedin']) ? $config['linkedin'] : 0,
    ];

    $form['pinterest'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Pinterest'),
      '#default_value' => isset($config['pinterest']) ? $config['pinterest'] : 0,
    ];

    $form['whatsapp'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Whatsapp'),
      '#default_value' => isset($config['whatsapp']) ? $config['whatsapp'] : 0,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['facebook'] = $values['facebook'];
    $this->configuration['twitter'] = $values['twitter'];
    $this->configuration['linkedin'] = $values['linkedin'];
    $this->configuration['pinterest'] = $values['pinterest'];
    $this->configuration['whatsapp'] = $values['whatsapp'];
  }

}
