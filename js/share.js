(function($, window, document, Drupal) {

	function SocialShare() {
		this.init();
	}

	SocialShare.prototype.init = function() {
		this.attachHandlers();
	}

	SocialShare.prototype.attachHandlers = function() {
		var socialShare = this;
		$('.share__link').each(function() {
			$(this).on('click', function(event) {
				event.preventDefault();
				socialShare.openPopup($(this).attr('href'));
			});
		});
	}
	SocialShare.prototype.openPopup = function(url) {
	    var left = (screen.width - 570) / 2;
	    var top = (screen.height - 570) / 2;
	    var params = 'menubar=no,toolbar=no,status=no,width=570,height=570,top=' + top + ',left=' + left;
	    window.open(url, 'ShareWindow', params);
	}

	$(function() {
		var social = new SocialShare;
	});

})(jQuery, window, document, Drupal);

